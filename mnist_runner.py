import os.path
from PIL import Image
import parser2

import tensorflow as tf
sess = tf.InteractiveSession()
def wV(shape):
  return tf.Variable(tf.truncated_normal(shape, stddev=0.1))
def bV(shape):
  return tf.Variable(tf.constant(0.1, shape=shape))
def c2(x, W):
  return tf.nn.conv2d(x,W,[1, 1, 1, 1],'SAME')
def mp22(x):
  return tf.nn.max_pool(x,[1, 2, 2, 1],[1, 2, 2, 1],'SAME')
x = tf.placeholder(tf.float32, shape=[None, 784])
y_ = tf.placeholder(tf.float32, shape=[None, 10])
wc1 = wV([5, 5, 1, 32])
bc1 = bV([32])
xp = tf.reshape(x, [-1,28,28,1])
hc1 = tf.nn.relu(c2(xp, wc1) + bc1)
hp1 = mp22(hc1)

wc2 = wV([5, 5, 32, 64])
bc2 = bV([64])
hc2 = tf.nn.relu(c2(hp1, wc2) + bc2)
hp2 = mp22(hc2)

we1 = wV([7 * 7 * 64, 1024])
be1 = bV([1024])
hp2f = tf.reshape(hp2, [-1, 7 * 7 * 64])
he1 = tf.nn.relu(tf.matmul(hp2f, we1) + be1)

we2 = wV([1024, 10])
be2 = bV([10])
yc=tf.nn.softmax(tf.matmul(he1, we2) + be2)

correct_prediction = tf.equal(tf.argmax(yc,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
answers = tf.argmax(yc,1)
saver = tf.train.Saver()

sess.run(tf.initialize_all_variables())
saver.restore(sess, "./ckpts/modelold.ckpt")
p = parser2.parser2()
batch = p.parse(150)
ansret = answers.eval({x:batch})
f = open('datatmp/outfile','w')
ct = 1
for i in ansret:
  f.write(str(i))
  if(ct%5 == 0):
    f.write('\n')
  ct+=1





