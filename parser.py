import sys
import cv2
import numpy as np
import math
from PIL import Image, ImageEnhance

def extract(i, j, id):
   xd = 68
   yd = 68
   mxx,mxy = im.size
   jm = im.crop((i,j, min(i+xd,mxx-1), min(j+yd,mxy-1)))
   xd,yd = jm.size
   enhancer = ImageEnhance.Contrast(jm)
   e2 = ImageEnhance.Brightness(jm)
   jm = enhancer.enhance(3)
   for a in range(xd):
      for b in range(yd):
         if(jm.getpixel((a,b)) > 128):
            jm.putpixel((a,b), 255)
         else:
            jm.putpixel((a,b), 0)
   ll = 0
   ul = 0
   lr = 0
   ur = 0
   for l1 in range(xd):
      for k in range(yd):
         if(jm.getpixel((l1,k)) < 200):
            ll = l1
            break
   for l1 in reversed(range(xd)):
      for k in range(yd):
         if(jm.getpixel((l1,k)) < 200):
            lr = l1
            break
   for l1 in range(yd):
      for k in range(xd):
         if(jm.getpixel((k,l1)) < 200):
            ul = l1
            break
   for l1 in reversed(range(yd)):
      for k in range(xd):
         if(jm.getpixel((k,l1)) < 200):
            ur = l1
            break
   jm = jm.crop((lr-1, ur-1, ll+1, ul+1))
   js = jm.size
   mx = max(js)
   hfx = (int(28-float(js[0])/mx*20.0)/2)
   hfy = (int(28-float(js[1])/mx*20.0)/2)
   jm = jm.resize((int(float(js[0])/mx*20.0), int(float(js[1])/mx*20.0)), Image.ANTIALIAS)
   xrs = 0.0
   w1 = 0.0
   yrs = 0.0
   for wi in range(int(float(js[0])/mx*20.0)):
      for wj in range(int(float(js[1])/mx*20.0)):
         xrs += wi*(1-jm.getpixel((wi,wj))/255.0)
         w1 += 1-jm.getpixel((wi,wj))/255.0
         yrs += wj*(1-jm.getpixel((wi,wj))/255.0)
   xoff = int(int(float(js[0])/mx*10.0)-(xrs/w1))
   yoff = int(int(float(js[1])/mx*10.0)-(yrs/w1))
   pm = Image.new("L",(28,28), 255)
   for imit in range(int(float(js[0])/mx*20.0)):
      for imjt in range(int(float(js[1])/mx*20.0)):
         pm.putpixel((max(min(imit+hfx+xoff,27),0), max(min(imjt+hfy+yoff,27),0)),jm.getpixel((imit, imjt)))
   pm.save("datatmp/"+str(id)+'.png')

def block(which):
   xd = 68
   yd = 68
   for j in range(5):
      for i in range(5):
         extract(i*xd + bx[which], j*yd + by[which],which*25 + j*5+i)

##find circles
im = Image.open(sys.argv[1])
im = im.convert("L")

img = cv2.imread(sys.argv[1],0)
(thresh, img) = cv2.threshold(img, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,100,param1=30,param2=15,minRadius=62,maxRadius=70)
circles = circles[0]
circles = circles[circles[:,1].argsort()]
for a in circles:
   im.putpixel((a[0],a[1]), 255)
im.save(sys.argv[1])
print circles

##remove lines
image1 = cv2.imread(sys.argv[1])
gray=cv2.cvtColor(image1,cv2.COLOR_BGR2GRAY)
dst = cv2.Canny(gray, 50, 200)

lines= cv2.HoughLines(dst,rho=1,theta=math.pi/180.0, threshold=270)

a,b,c = lines.shape
for i in range(a):
    rho = lines[i][0][0]
    theta = lines[i][0][1]
    a = math.cos(theta)
    b = math.sin(theta)
    x0, y0 = a*rho, b*rho
    pt1 = ( min(int(x0+1500*(-b)),1100), int(y0+1500*(a)))
    pt2 = ( min(int(x0-1500*(-b)),1100), int(y0-1500*(a)))
    cv2.line(image1, pt1, pt2, (255, 255, 255), 4, cv2.LINE_AA)


cv2.imwrite(sys.argv[1], image1)

## blocking

im = Image.open(sys.argv[1])
im = im.convert("L")

bx = [circles[0][0] - 6*68, circles[1][0] - 6*68, circles[2][0] - 6*68, circles[0][0] + 2*68, circles[1][0] + 2*68, circles[2][0] + 2*68]
by = [circles[0][1] - 68, circles[1][1] - 68, circles[2][1] - 68, circles[0][1] - 68, circles[1][1] - 68, circles[2][1] - 68]

for i in range(6):
   block(i)


