# README #

* A test grader that uses openCV to process test documents and a neural net to grade documents. Test document can be found under ansdoc.pdf under mlgrade/Downloads
* ansdoc.pdf should be scanned at 150dpi

* To use:
*     rename all documents to be scan_<int>.png
*     keep all files in same directory as python files
*     run runner.py
*     output will be in outfiles directory.

* Program was used to grade tests on a dev machine - may need some slight modifications (setup directory, install open cv, etc.) for external use

* Version 0.2

* Albert Tseng 