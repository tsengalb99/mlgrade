import sys
n = int(sys.argv[1])
s = open("score.csv",'w')
for i in range(n):
    f = open("datatmp/outfile"+str(i), 'r')
    a = open("ans", 'r')
    ct = 0
    for j in range(30):
        t1 = int(f.readline())
        t2 = int(a.readline())
        if(t1 == t2):
            ct +=1
    s.write(f.readline() + "," + str(ct) + "\n")
