import sys
import cv2
import numpy as np
import math

image1 = cv2.imread(sys.argv[1])
gray=cv2.cvtColor(image1,cv2.COLOR_BGR2GRAY)
dst = cv2.Canny(gray, 50, 200)

lines= cv2.HoughLines(dst,rho=1,theta=math.pi/180.0, threshold=270)

a,b,c = lines.shape
for i in range(a):
    rho = lines[i][0][0]
    theta = lines[i][0][1]
    a = math.cos(theta)
    b = math.sin(theta)
    x0, y0 = a*rho, b*rho
    pt1 = ( min(int(x0+1500*(-b)),1100), int(y0+1500*(a)))
    pt2 = ( min(int(x0-1500*(-b)),1100), int(y0-1500*(a)))
    cv2.line(image1, pt1, pt2, (255, 255, 255), 2, cv2.LINE_AA)


cv2.imwrite(sys.argv[1], image1)
