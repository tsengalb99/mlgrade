import numpy as np, cv2, math, sys
from PIL import Image
from scipy import stats

img = cv2.imread(sys.argv[1],0)
height, width = img.shape
(thresh, img) = cv2.threshold(img, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,300,param1=30,param2=15,minRadius=62,maxRadius=70)
circles = circles[0]
circles = circles[circles[:,1].argsort()]
circles = circles[circles[:,0].argsort()]
print(circles)
x2 = [circles[0][0], circles[1][0]];
y2 = [circles[0][1], circles[1][1]];
x3 = [circles[5][0], circles[6][0]];
y3 = [circles[5][1], circles[6][1]];
slope, intercept, r_value, p_value, std_err = stats.linregress(y2,x2)
slope2, intercept, r_value, p_value, std_err = stats.linregress(y3,x3)
slope = (slope + slope2)/2.0
im = Image.open(sys.argv[1])
if(slope < 1000):
    im = im.rotate(-1.0*math.degrees(math.atan(slope)), Image.BILINEAR)
im.save(sys.argv[1])
im = im.crop((150, 150, 1080, 1300))
im.save(sys.argv[1])

