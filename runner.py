import sys
import os

n = sys.argv[1]
print(n)
os.system("mkdir outfiles")

for i in range(int(float(n)) + 1):
    fname = str("scan_" + str(i) + ".png")
    os.system("python normalizer.py " + fname)
    os.system("python parser.py " + fname)
    os.system("python mnist_runner.py")
    os.system("cp datatmp/outfile outfiles/" + str(i))

